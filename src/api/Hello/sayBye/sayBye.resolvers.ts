const resolvers = {
  Query: {
    sayBye: () => "Bye",
  },
};

export default resolvers;
