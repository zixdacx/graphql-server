export const typeDefs = ["type Query {\n  sayBye: String!\n  sayHello(name: String!): Hello!\n}\n\ntype Hello {\n  text: String!\n  error: Boolean!\n}\n"];
/* tslint:disable */

export interface Query {
  sayBye: string;
  sayHello: Hello;
}

export interface SayHelloQueryArgs {
  name: string;
}

export interface Hello {
  text: string;
  error: boolean;
}
