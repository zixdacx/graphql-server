import { Hello, SayHelloQueryArgs } from "../../../types/graph";

const resolvers = {
  Query: {
    sayHello: (_, args: SayHelloQueryArgs): Hello => {
      return {
        text: `supsup${args.name}`,
        error: false,
      };
    },
  },
};
export default resolvers;
